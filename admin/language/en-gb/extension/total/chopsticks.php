<?php
// Heading
$_['heading_title'] = 'Pay for one pair of chopsticks';

// Text
$_['text_extension']   = 'One pair of chopsticks is free for any sushi set';
$_['text_success']     = 'Success: You have modified account module!';
$_['text_edit']        = 'Edit Account Module';


// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify account module!';
