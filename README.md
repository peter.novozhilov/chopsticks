# Chopsticks opencart module

class: ModelExtensionTotalChopsticks
path: /catalog/module/extension/total/chopsticks.php
opencart: v 2.3
php: 7.4+

Tech stack: PHP, MySQL, HTML, Vagrant, Git, Opencart, MVC, OOP. This module adds the number
of persons to the field of open cart and it counts the price of chopsticks if we have products from concrete categories. It took me 3 hours to write this script for https://resto-presto.com.ua. It can be checked in the categories: wok and sushi. You can try to add some products to the cart from these categories, and you will be able to see the "number of persons" field at the checkout step. Then you can answer the questions: How many chopsticks will you need? How much it will cost? 
