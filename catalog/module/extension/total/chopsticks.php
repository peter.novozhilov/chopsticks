<?php

/**
 * @const DB_PREFIX - This prefix of the database was defined in config.php at the root of the site.
 * @class This class adds the number of person field to the opencart totals and counts the price of
 * chopsticks if we have products from concrete categories. It takes me 3 hours to write such a script for
 * https://resto-presto.com.ua. You can check it in the categories: wok and sushi. You should try to add some
 * products to the cart from these categories, and You will see the number of person field.
 * Then You can answer the question: how many chopsticks will You need? And how much it will cost?
 * @version This script was tested at opencart 2.3
 * @php 7.4
 */

class ModelExtensionTotalChopsticks extends Model
{

    /* Below we will store some magic numbers. */
    /* Here is an id of the chopsticks product, and we will use its price further. */
    const CHOPSTICKS_PRODUCT_ID = 214;
    /* Here are the category ids. Their products we will use with a number of person field in the cart (for example sushi, wok). */
    const CHOPSTICKS_CATEGORIES_ID = '15,14';
    /* How many chopsticks will we offer for free by default? */
    const CHOPSTICKS_DEFAULT_PERSONS = 1;
    /* In which attribute of the product will we store the count of free chopsticks? */
    const CHOPSTICKS_QATTR_ID = 11;

    /**
     * We will store here the template of a number of persons/chopsticks field.
     * It's dirty, but opencart has no template for the totals model.
     */

    const COPSTICKS_TMPL = '<input class="form-control" id="sushi-persons" type="number" data-onchange="reloadAll" name="persons" value="%s" size="1" />';

    /**
     * @function priceOfChopstick - It gets the price of one pair of chopsticks.
     * @params void
     * @return float $price - It returns the price of one pair of chopsticks.
     */

    private function priceOfChopstick():float
    {
        $price = 0;

        $price_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product 
                    WHERE product_id = '" . (int)self::CHOPSTICKS_PRODUCT_ID . "'");

        if ($price_query->row
            && isset($price_query->row['price'])
            && $price_query->row['price'] > 0
        ) {

            $price = $price_query->row['price'];

        }

        return $price;

    }

    /**
     * @function countSushiByAttr - It gets a free amount of chopsticks from the cart product attribute.
     * @params int $iProductId - Here is the cart product id, and we will check its attributes for free chopsticks.
     * @return int $iSushiCountAttr - It returns a free amount of chopsticks from the cart product attribute.
     */

    private function countSushiByAttr(int $iProductId):int
    {

        $iSushiCountAttr = 1;

        $issushi_query = $this->db->query("SELECT text as count FROM " . DB_PREFIX . "product_attribute 
                    WHERE product_id = '" . (int)$iProductId . "' 
                    AND attribute_id = '" . (int)self::CHOPSTICKS_QATTR_ID . "'
                    AND language_id = '" . (int)$this->config->get('config_language_id') . "' ");

        if ($issushi_query->row
            && isset($issushi_query->row['count'])
            && $issushi_query->row['count'] > 0
        ) {

            $iSushiCountAttr = (int)trim($issushi_query->row['count']);
            $iSushiCountAttr = $iSushiCountAttr > 0 ? $iSushiCountAttr : 1;
        }

        return $iSushiCountAttr;


    }

    /**
     * @function countSushi - It counts a free amount of chopsticks from all cart products.
     * @params void
     * @return int $iSushiCountAttr - It returns a free amount of chopsticks from all cart products.
     */

    private function countSushi():int
    {

        $iSushiCount = 0;
        $products = $this->cart->getProducts();

        foreach ($products as $product) {

            $issushi_query = $this->db->query("SELECT COUNT(product_id) as count FROM " . DB_PREFIX . "product_to_category 
                    WHERE product_id = '" . (int)$product['product_id'] . "' 
                    AND category_id IN (" . self::CHOPSTICKS_CATEGORIES_ID . ")");

            if ($issushi_query->row
                && isset($issushi_query->row['count'])
                && $issushi_query->row['count'] > 0
            ) {

                $iCountSushiByAttr = $this->countSushiByAttr($product['product_id']);
                $iSushiCount += $iCountSushiByAttr * $product['quantity'];

            }

        }

        return $iSushiCount;

    }

    /**
     * @function getTotalPriceOfChopsticks - It counts all costs of chopsticks in the cart.
     * @params float $price - It's the price of one pair of chopsticks.
     * @params int $persons - How many people do we have?
     * @params int $countSushi - How many free chopsticks pairs do we have?
     * @return float $price  - It returns all costs of chopsticks in the cart.
     */

    private function getTotalPriceOfChopsticks(float $price, int $persons, int $countSushi):float
    {
        $price = ($persons - $countSushi) * $price;
        $price = $price > 0 ? $price : 0;
        return $price;
    }


    /**
     * @function getTotal - It adds a person field to all totals and counts all costs of chopsticks in the cart.
     * @params array $total - It's a reference for all totals and subtotals in a cart, and we will modify it further.
     * @return void
     */

    public function getTotal(array $total)
    {

        $this->load->language('extension/total/chopsticks');

        $iCountSushi = $this->countSushi();

        if ($iCountSushi > 0) {

            $iPrice = $this->priceOfChopstick();

            $iPersons = isset($this->request->post['persons']) ? $this->request->post['persons'] : self::CHOPSTICKS_DEFAULT_PERSONS;
            $iPersons = (int)$iPersons;

            $sNumberInput = sprintf(self::COPSTICKS_TMPL, $iPersons);

            $title = sprintf($this->language->get('text_persons'), $iPersons) . $sNumberInput;

            $iAmount = $this->getTotalPriceOfChopsticks($iPrice, $iPersons, $iCountSushi);

            $total['totals'][] = array(
                'code' => 'chopsticks',
                'title' => $title,
                'value' => $iAmount,
                'sort_order' => $this->config->get('chopsticks_sort_order')
            );

            $total['total'] += $iAmount;

        }


    }

}
