<?php
// Heading
$_['heading_title'] = 'Pay for one pair of chopsticks';

// Text
$_['text_persons'] = 'How many chopsticks do You need? (%s)';
$_['text_desc']  = 'One pair of chopsticks is free for any sushi set';

